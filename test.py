import json
from player import Player

with(open("gamemodel.json", 'r')) as f:
    raw_data = f.read()
    game_state = json.loads(raw_data)
    response = Player().betRequest(game_state)
