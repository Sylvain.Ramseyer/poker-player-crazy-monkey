
class Player:
    VERSION = "16"

    letterToNumber = {
        'J': 11,
        'Q': 12,
        'K': 13,
        'A': 14
    }

    def betRequest(self, game_state):
        our_player_id =  game_state['in_action']
        our_player = game_state['players'][our_player_id]
        our_cards = our_player['hole_cards']
        community_cards = game_state['community_cards']
        follow_amount = game_state['current_buy_in'] - our_player['bet']
        all_in_amount = our_player['stack']

        combination_list = [
            [self.isRoyalFlush,    100, 2500000],
            [self.isStraightFlush, 100,  250000],
            [self.isFourKind,      100,    2500],
            [self.isFullHouse,     100,     500],
            [self.isFlush,         100,     400],
            [self.isStraight,      100,     200],
            [self.isThreeKind,     100,      20],
            [self.isTwoPair,        75,      10],
            [self.isOnePair,        25,       2],
            [self.isHighCard,        0,       1]
        ]

        probability = 0
        amount = 0

        try:
            print("======================START LOG OF ROUND======================")
            print("Hand:")
            self.printCards(our_cards)
            print("Community:")
            self.printCards(community_cards)
            print("Current bet:", our_player['bet'])
            print("Follow amount:", follow_amount)
            print("All-in amount:", all_in_amount)

            amount_multiplier = 0
            dic_result_with_weight = {}

            for combination in combination_list:
                result = combination[0](our_cards, community_cards)
                result_with_weight = combination[2] * result
                probability = probability + result_with_weight
                dic_result_with_weight[result_with_weight] = combination
                print("Function " + combination[0].__name__ + " with probability of " + str(result))

            max_key = max(dic_result_with_weight.keys(), key=int)
            max_record = dic_result_with_weight[max_key]
            amount_multiplier =max_record[1]

            print("Weighted probability: " + str(probability))

            if probability >= 2 or self.isReasonableAmount(probability, follow_amount):
                amount_chosen = amount_multiplier * ((all_in_amount - follow_amount) / 100) + follow_amount
                amount = self.normalizeAmount(amount_chosen, all_in_amount)

            print("Amount multiplier: " + str(amount_multiplier))
            print("Amount: " + str(amount))
            print("======================END LOG OF ROUND======================")
            return amount
        except:
            raise
            return all_in_amount

    def showdown(self, game_state):
        pass

    def isRoyalFlush(self, player_cards, community_cards):
        return 0

    def isStraightFlush(self, player_cards, community_cards):
        return 0

    def isFourKind(self, player_cards, community_cards):
        list_kind = { }
        all_cards = player_cards + community_cards

        for card in all_cards:
            if not card['rank'] in list_kind:
                list_kind[card['rank']] = 1
            else:
                list_kind[card['rank']] += 1

        for kind, number in list_kind.items():
            if number >= 4:
                return 1

        return 0

    def isFullHouse(self, player_cards, community_cards):
        list_kind = { }
        all_cards = player_cards + community_cards
        for card in all_cards:
            if not card['rank'] in list_kind:
                list_kind[card['rank']] = 1
            else:
                list_kind[card['rank']] += 1
        for i in list_kind:
            if list_kind[i] == 3:
                for y in list_kind:
                    if list_kind[y] == 2:
                        return 1
        return 0

    def isFlush(self, player_cards, community_cards):
        all_cards = player_cards + community_cards
        list_suit = { }

        for card in all_cards:
            if not card['suit'] in list_suit:
                list_suit[card['suit']] = 1
            else:
                list_suit[card['suit']] += 1

        for suit, number in list_suit.items():
            if number >= 5:
                return 1

        return 0

    def isStraight(self, player_cards, community_cards):
        all_cards = player_cards + community_cards
        all_cards_numbers = []
        for index, card in enumerate(all_cards):
            all_cards_numbers.append(self.mapCardToNumber(card['rank']))
        sorted_cards = sorted(all_cards_numbers)
        for idx, val in enumerate(sorted_cards):
            counter = 1
            while (val+counter) in sorted_cards:
                counter = counter + 1
                if counter >= 5:
                    return 1
        return 0

    def isThreeKind(self, player_cards, community_cards):
        list_kind = { }
        all_cards = player_cards + community_cards

        for card in all_cards:
            if not card['rank'] in list_kind:
                list_kind[card['rank']] = 1
            else:
                list_kind[card['rank']] += 1

        for kind, number in list_kind.items():
            if number >= 3:
                return 1

        return 0

    def isTwoPair(self, player_cards, community_cards):
        all_cards = player_cards + community_cards
        pair_number = 0

        if player_cards[0]['rank'] == player_cards[1]['rank']:
            pair_number = pair_number + 1

        for index, card in enumerate(player_cards):
            for index_other, card_other in enumerate(community_cards):
                if card['rank'] == card_other['rank']:
                    pair_number = pair_number + 1

        if pair_number >= 2:
            return 1

        return 0

    def isOnePair(self, player_cards, community_cards):
        if player_cards[0]['rank'] == player_cards[1]['rank']:
            return 1

        for index, card in enumerate(player_cards):
            for index_other, card_other in enumerate(community_cards):
                if card['rank'] == card_other['rank']:
                    return 1

        return 0

    def mapCardToNumber(self, card):
        if card in self.letterToNumber:
            return int(self.letterToNumber[card])
        return int(card)

    def isHighCard(self, player_cards, community_cards):
        good_card = ['J', 'Q', 'K', 'A']
        for card in player_cards:
            if card['rank'] in good_card:
                return 1

        return 0

    def isReasonableAmount(self, probability, amount):
        if amount < 100:
            return 1

        return 0

    def normalizeAmount(self, amount, maximum):
        maximum = int(maximum)
        amount = int(amount)
        amount = max(0, amount)
        amount = min(amount, maximum)
        return amount

    def printCards(self, player_cards):
        for index, card in enumerate(player_cards):
            print(" * " + card['rank'] + " of " + card['suit'])
