
class Player:
    VERSION = "1"

    def betRequest(self, game_state):
        our_player_id =  game_state['in_action']
        our_player = game_state['players'][our_player_id]
        our_cards = our_player['hole_cards']
        community_cards = game_state['community_cards']

        combination = {
            2500000: Player().isRoyalFlush,
            250000: Player().isStraightFlush,
            2500: Player().isFourKind,
            500: Player().isFullHouse,
            400: Player().isFlush,
            200: Player().isStraight,
            20: Player().isThreeKind,
            10: Player().isTwoPair,
            2: Player().isOnePair
        }

        probability = 0

        for weight, function in combination.items():
            probability = probability + (weight * function(our_cards, community_cards))

        if probability >= 2:
            return game_state['current_buy_in'] - our_player['bet']
        else:
            return 0

    def showdown(self, player_cards, community_cards):
        pass

    def isRoyalFlush(self, player_cards, community_cards):
        return 0

    def isStraightFlush(self, player_cards, community_cards):
        return 0

    def isFourKind(self, player_cards, community_cards):
        return 0

    def isFullHouse(self, player_cards, community_cards):
        return 0

    def isFlush(self, player_cards, community_cards):
        return 0

    def isStraight(self, player_cards, community_cards):
        return 0

    def isThreeKind(self, player_cards, community_cards):
        return 0

    def isTwoPair(self, player_cards, community_cards):
        return 0

    def isOnePair(self, player_cards, community_cards):
        all_cards = player_cards + community_cards

        for index, card in enumerate(all_cards):
            for index_other in range(index + 1, len(all_cards)):
                if card['rank'] == all_cards[index_other]['rank']:
                    return 1

        return 0

    def isHighCard(self, player_cards, community_cards):
        return 0
